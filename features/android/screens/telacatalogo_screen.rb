# Tela de catalogo
class TelaCatalogoScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'catalog_activity' }
  element(:produto)             { 'product_item' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

  def existem_produtos?
    # sleep(2)
    wait_for(timeout: 5) { element_exists("* id:'#{produto}'") }
    produtos = query("* id:'#{produto}'")

    puts produtos.length >= 1
    return produtos.length >= 1
  end

  def tocar_produto
    query = "* id:'#{produto}'" if query.nil?
    begin
      wait_for(timeout: 20) { element_exists(query) }
      touch(query)
    rescue => e
      raise "Problema ao tocar o elemento da tela: '#{produto}'\nError Message: #{e.message}"
    end
  end
end

# Tela inicial do app
class TelaInicialScreen < AndroidScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'home_activity' }
  element(:banner)              { 'category_home_item_layout' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* marked:'#{button}'")
  # end

  def existem_banners?
    banners = query("* id:'#{banner}'")
    return banners.length >= 1
  end

  def tocar_banner
    query = "* id:'#{banner}'" if query.nil?
    begin
      wait_for(timeout: 5) { element_exists(query) }
      touch(query[0])
    rescue => e
      raise "Problem in touch screen element: '#{banner}'\nError Message: #{e.message}"
    end
  end
end

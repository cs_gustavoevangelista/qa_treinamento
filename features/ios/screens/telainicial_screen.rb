class TelaInicialScreen < IOSScreenBase
  # Identificador da tela
  trait(:trait)                 { "* marked:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'HOME_SCREEN' }
  element(:banner)              { 'BANNER_LIST' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* marked:'#{button}'")
  # end

  def existem_banners?
    banners = query("* marked:'#{banner}'")
    return banners.length >= 1
  end

  def tocar_banner
    query = "* id:'#{banner}'" if query.nil?
    begin
      wait_for(timeout: 5) { element_exists(query) }
      touch(query[0])
    rescue => e
      raise "Problem in touch screen element: '#{banner}'\nError Message: #{e.message}"
    end
  end
end

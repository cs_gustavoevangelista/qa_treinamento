#
class TelaDetalheScreen < IOSScreenBase
  # Identificador da tela
  trait(:trait)                 { "* id:'#{layout_name}'" }

  # Declare todos os elementos da tela
  element(:layout_name)         { 'PRODUCT_SCREEN' }
  element(:size_button)         { 'button_size' }
  element(:color_button)        { 'button_color' }
  element(:cart_button)         { 'buy_button' }
  element(:size_list_item)      { 'text1' }
  element(:color_list_item)     { 'text1' }

  # Declare todas as acoes da tela
  # action(:touch_button) do
  #   touch("* id:'#{button}'")
  # end

  def clicar_tamanho
    touch("* id:'#{size_button}'")
  end

  def escolher_tamanho(tamanho)
    touch("* marked:'#{tamanho}'")
  end

  def clicar_cor
    touch("* id:'#{color_button}'")
  end

  def escolher_cor(cor)
    touch("* marked:'#{cor}'")
  end

  def ver_cor_na_tela(cor)
    puts query("* {text CONTAINS[c] '#{cor}'}").nil?
    query("* {text CONTAINS[c] '#{cor}'}").nil?
  end

  def ver_tamanho_na_tela(tamanho)
    puts query("* {text CONTAINS[c] '#{tamanho}'}").nil?
    query("* {text CONTAINS[c] '#{tamanho}'}").nil?
  end

  def clicar_carrinho
    touch("* id:'#{cart_button}'")
  end

  def validar_mensagem_erro
    puts query("* id:'message'").nil?
    puts query("* id:'message'").length
    query("* id:'message'").nil?
  end
end

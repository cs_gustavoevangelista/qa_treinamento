# language: pt
Funcionalidade: Tela Inicial
A tela inicial deve conter ao menos um banner
Se eu tocar em um banner, eu devo ver a tela de catálogo

  @reinstall @ios
  Cenário: Ao menos um banner na tela Inicial
  Dado que estou na tela inicial
  Entao tenho que ver ao menos um banner

  @ios
  Cenário: Clicar no banner e ser redirecionado para tela de catálogo
  Dado que estou na tela inicial
  Quando toco no banner
  Então devo visualizar a tela de catálogo

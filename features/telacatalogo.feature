# language: pt
Funcionalidade: Tela Catalogo
O catálogo deve conter ao menos um produto
Se eu tocar em um produto, eu posso ver seus detalhes

  
  Contexto:
  Dado que estou na tela inicial
  Quando toco no banner

  @reinstall @ios
  Cenário: Ao menos um produto na Tela de Catalogo
  Dado que estou na tela de catalogo
  Entao tenho que ver ao menos um produto

  @ios
  Cenário: Clicar no produto e ser redirecionado para Tela de Detalhes
  Dado que estou na tela de catalogo
  Quando toco no produto
  Então devo visualizar a tela de detalhes

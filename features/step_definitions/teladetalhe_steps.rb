######### DADO #########
Dado(/^que estou na Tela de Detalhe do produto$/) do
  @page_detalhe = page(TelaDetalheScreen).await(timeout: 5)
end

######### QUANDO #########
Quando(/^escolho o tamanho ([^"]*) e a cor ([^"]*)$/) do |tamanho, cor|
  @page.drag_to 'baixo'.to_sym
  @page_detalhe.clicar_tamanho
  @page_detalhe.escolher_tamanho tamanho
  @page_detalhe.clicar_cor
  @page_detalhe.escolher_cor cor
end

Quando(/^incluo o produto no carrinho$/) do
  @page_detalhe.clicar_carrinho
end

Quando(/^clico para selecionar uma cor$/) do
  @page.drag_to 'baixo'.to_sym
  @page_detalhe.clicar_cor
end

Quando(/^o tamanho ([^"]*) e a cor ([^"]*) sao vistos na tela$/) do |tamanho, cor|
  fail 'Nao tem tamanho na tela' if @page_detalhe.ver_tamanho_na_tela tamanho == true
  fail 'Nao tem cor na tela' if @page_detalhe.ver_cor_na_tela cor == true
end

######### ENTAO #########
Então(/^devo visualizar a tela de detalhes$/) do
  page(TelaDetalheScreen).await(timeout: 5)
end

Entao(/^é informado uma mensagem de erro$/) do
  sleep(3)
  fail 'nao tem mensagem de erro' if @page_detalhe.validar_mensagem_erro == true
end

Entao(/^devo visualizar a tela do carrinho$/) do
  page(TelaCarrinhoScreen).await(timeout: 5)
end

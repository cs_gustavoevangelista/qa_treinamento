######### DADO #########
Dado(/^que estou na tela inicial$/) do
  @page = page(TelaInicialScreen).await(timeout: 5)
end

######### QUANDO #########
Quando(/^toco no banner$/) do
  @page.tocar_banner
end

######### ENTAO #########
Entao(/^tenho que ver ao menos um banner$/) do
  fail 'Não há banners suficientes na tela' if
    @page.existem_banners? == false
end

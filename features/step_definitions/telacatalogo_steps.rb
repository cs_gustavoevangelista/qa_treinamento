######### DADO #########
Dado(/^que estou na tela de catalogo$/) do
  @page_catalogo = page(TelaCatalogoScreen).await(timeout: 5)
end

######### QUANDO #########
Quando(/^toco no produto$/) do
  @page_catalogo.tocar_produto
end

######### ENTAO #########
Então(/^devo visualizar a tela de catálogo$/) do
  page(TelaCatalogoScreen).await(timeout: 20)
end

Entao(/^tenho que ver ao menos um produto$/) do
  fail 'Não há produtos suficientes na tela' if
    @page_catalogo.existem_produtos? == false
end

# language: pt
Funcionalidade: Tela de Detalhe do Produto
Posso escolher o tamanho e a cor do produto
Só posso incluir o produto no carrinho se escolher seu tamanho e cor, caso contrário devo ver uma mensagem de erro.
Só posso selecionar uma cor se selecionar um tamanho previamente, caso contrário devo ver uma mensagem de erro.


  Contexto:
  Dado que estou na tela inicial
  Quando toco no banner
  Dado que estou na tela de catalogo
  Quando toco no produto

  Cenário: Escolher o tamanho e a cor do produto
  Dado que estou na Tela de Detalhe do produto
  Quando escolho o tamanho 40 e a cor Preto
  E o tamanho 40 e a cor Preto sao vistos na tela
  E incluo o produto no carrinho
  Entao devo visualizar a tela do carrinho


  Cenário: Ver mensagem de erro ao escolher comprar sem ter selecionado tamanho e cor
  Dado que estou na Tela de Detalhe do produto
  Quando incluo o produto no carrinho
  Entao é informado uma mensagem de erro


  Cenário: Ver mensagem de erro ao escolher uma cor sem ter selecionado o tamanho
  Dado que estou na Tela de Detalhe do produto
  Quando clico para selecionar uma cor
  Entao é informado uma mensagem de erro
